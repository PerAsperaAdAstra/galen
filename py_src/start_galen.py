#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
start_galen.py

This script is to demonstrate start
GALEN

'''

# import modules
import os
import time
import socket
import pyttsx3
import subprocess
import speech_recognition as sr
from pocketsphinx import LiveSpeech



#tts function
def speak2me(the_text):

    print(the_text)
    #init TTS
    engine = pyttsx3.init()

    engine.say(the_text)
    engine.runAndWait()



def listen_for_input():
    print("ready")
    # obtain audio from the microphone
    r = sr.Recognizer()
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source)
        audio = r.listen(source)

    # recognize speech using Sphinx
    voice_data = ''
    try:
        google_out = r.recognize_google(audio)
        speak2me("Google thinks you said " + google_out)
        voice_data = google_out.lower()

        #sphinx_out = r.recognize_sphinx(audio)
        #speak2me("Sphinx thinks you said " + sphinx_out)
        #voice_data = sphinx_out.lower()

    except sr.UnknownValueError:
        print("Sphinx could not understand audio")
        #speak2me("Sphinx could not understand audio")

    except sr.RequestError as e:
        print("Sphinx error; {0}".format(e))
        speak2me("Sphinx error; {0}".format(e))


    return voice_data


def respond(voice_data, convo_flag):

    if "galen" in voice_data:
        speak2me("I am listening")
        convo_flag = True
    elif convo_flag == True:
        if "time" in voice_data:
            year, month, day, hour, min = map(int, time.strftime("%Y %m %d %H %M").split())
            speak2me("It is " + str(hour) + " " + str(min))
            convo_flag = False

        #TBD add more stuff here




    return convo_flag


#main code
speak2me("Galen starting")
time.sleep(0.25)

# connect to the host -- tells us if the host is actually reachable
inet_connection = False
try:
    socket.create_connection(("1.1.1.1", 53))
    inet_connection = True
    speak2me("Connected to the internet")
    time.sleep(0.25)

except OSError:
    inet_connection = False
    speak2me("Internet connection not detected")
    time.sleep(0.25)


#start ROOTIN
subprocess.Popen(['python3 '+os.path.join(os.path.abspath(os.path.dirname(os.path.realpath(__file__))),'test_noise.py ')], shell=True, stdin=None, stdout=None, stderr=None)
time.sleep(1)


speak2me("Galen ready")
time.sleep(0.25)

convo_flag = False
time.sleep(1)

while True:
    if inet_connection == True:
        voice_data = listen_for_input()
        convo_flag = respond(voice_data, convo_flag)

    else:
        speech = LiveSpeech(lm=False, keyphrase='galen', kws_threshold=1e-20)
        for phrase in speech:
            print(phrase.segments(detailed=True))



