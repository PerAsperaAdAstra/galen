#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
timelapse_demo.py

This script is to demonstrate basic
timelapse capability for later integration
into main code
'''


# IMPORT MODULES
import time
import picamera
import os


# MAIN CODE

with picamera.PiCamera() as camera:

    camera.resolution = (1280, 720)
    n = 0
    try:
        os.makedirs('../data')
    except:
        pass

    while True:
        time.sleep(10)
        camera.capture('../data/timelapse'+str(n)+'.jpg')
        n+=1

