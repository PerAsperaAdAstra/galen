#!/usr/bin/python3

import os
import time
import RPi.GPIO as GPIO



PIR_PIN = 4
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR_PIN, GPIO.IN)

volume = 1
sound_file = '/home/pi/galen/py_src/the_root.wav'
#sound_file = os.path.abspath(sound_file)
print(sound_file)

while True:
    if GPIO.input(PIR_PIN):
        os.system('mplayer -nolirc ' + sound_file + '  -af volume='+str(volume))

    time.sleep(0.5)






