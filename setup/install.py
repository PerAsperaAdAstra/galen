#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
install.py

This script is intended to automatically install
all of the required software to operate the
GALEN system (all configurations) and configure
the pi to do so.
'''

# IMPORT MODULES
import os


# MAIN CODE

#update things
os.system('sudo apt-get update -y')
os.system('sudo apt-get upgrade -y')

#install the superior text editor
os.system('sudo apt-get install gedit -y')

#voice recognition
os.system('sudo apt-get install swig -y')
os.system('sudo apt-get install libasound2-dev -y')
os.system('sudo apt-get install libpulse-dev --yes')
os.system('pip3 install --upgrade SpeechRecognition')
os.system('pip3 install --upgrade pyaudio')
os.system('sudo apt-get install pocketsphinx -y')
os.system('sudo apt-get install python-pocketsphinx -y')
os.system('pip3 install --upgrade pocketsphinx')
os.system('pip3 install --upgrade pyttsx3')
os.system('sudo apt-get install -y python-espeak')
os.system('sudo apt-get install -y flac')

#update audio for USB devices
os.system('sudo apt-get --yes install mplayer')
os.system('sudo apt-get --yes install pulseaudio')

#do speaker stuff
os.system('sudo mv /usr/share/alsa/alsa.conf /usr/share/alsa/alsa.conf_ORIGINAL')
os.system('sudo cp /home/pi/galen/setup/alsa.conf /usr/share/alsa/alsa.conf')

#copy over rc.local
os.system('sudo mv /etc/rc.local /etc/rc.local_OLD ')
os.system('sudo cp /home/pi/galen/setup/rc.local /etc/rc.local ')

print("\n\ndon't forget to enable the cam in raspi-config to enable timelapse!")



